<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 7/25/18
 * Time: 14:07
 */

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class IndexController
{

    public function index()
    {
        return new Response('It works.');
    }
}
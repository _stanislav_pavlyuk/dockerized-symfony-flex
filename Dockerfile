FROM php:7.2-fpm

ARG SYMFONY_ENV=dev
ENV SYMFONY_ENV ${SYMFONY_ENV}

ARG SYMFONY_DEBUG=1
ENV SYMFONY_DEBUG ${SYMFONY_DEBUG}

ADD ./docker/php/symfony.ini /usr/local/etc/php/conf.d/
ADD ./docker/php/php.ini /usr/local/etc/php/

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    curl \
    nginx \
    git \
    libmemcached-dev \
    libz-dev \
    libpq-dev \
    libjpeg-dev \
    libpng-dev \
    zip \
    libfreetype6-dev \
    libssl-dev \
    libmcrypt-dev \
  && rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-install pdo_mysql \
  && docker-php-ext-install pdo_pgsql \
  && docker-php-ext-install zip \
  && docker-php-ext-configure gd \
    --enable-gd-native-ttf \
    --with-jpeg-dir=/usr/lib \
    --with-freetype-dir=/usr/include/freetype2 && \
    docker-php-ext-install gd

# Enable and configure xdebug
RUN pecl install xdebug \
    && docker-php-ext-enable xdebug

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
	php /usr/local/bin/composer self-update

# Nginx isntallation & configuration
#RUN apt-get update && \
#  apt-get install -y software-properties-common && \
#  add-apt-repository -y ppa:nginx/stable && \
#  apt-get update && \
#  apt-get install -y nginx && \
#  rm -rf /var/lib/apt/lists/* && \
#  echo "\ndaemon off;" >> /etc/nginx/nginx.conf && \
#  chown -R www-data:www-data /var/lib/nginx

RUN usermod -u 1000 www-data
RUN chown -R www-data:www-data /var/lib/nginx

COPY ./docker/nginx/nginx.conf /etc/nginx/
COPY ./docker/nginx/sites /etc/nginx/sites-available

RUN echo "upstream php-upstream { server localhost:9000; }" > /etc/nginx/conf.d/upstream.conf;

WORKDIR /var/www/

COPY composer.json .
COPY composer.lock .
RUN composer install

COPY . .

RUN chmod 775 start.sh

# Expose ports.
EXPOSE 80 443

# Define default command.
CMD ["./start.sh"]

